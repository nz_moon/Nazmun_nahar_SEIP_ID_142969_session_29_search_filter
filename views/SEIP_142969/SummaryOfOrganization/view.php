<?php
require_once("../../../vendor/autoload.php");
use App\SummaryOfOrganization\SummaryOfOrganization;

$objSummaryOfOrganization=new SummaryOfOrganization();

$objSummaryOfOrganization->setData($_GET);
$oneData=$objSummaryOfOrganization->view("obj");

echo "ID :".$oneData->id."<br>";
echo "Organization Name:".$oneData->organization_name."<br>";
echo "Summary :".$oneData->organization_summary."<br>";